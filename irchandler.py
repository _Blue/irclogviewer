#!/usr/bin/python2
# -*- coding: utf-8 -*-

#***********************************************************************************
#This file is part of the LogReader project                                        *
#                                                                                  *
#This program is based on irclib and ircbot                                        *
#You are allowed to use, modify and redistribute this file as you like             *
#Note that this code comes without any guarantee and may be used at your own risks *
#                                                                                  *
#Blue Software, 2016                                                               *
#For more infos: https://bluecode.fr                                               *
#***********************************************************************************

from blueircbot import SingleServerIRCBot
import irclib
from threading import Event

VERSION = "1.0-release"
class IRCHandler(SingleServerIRCBot):

    def __init__(self, network, port, password,  channel, nickname, realname, username, usessl):
        self.channel = channel
        self.msg_queue = []
        self.name= nickname
        self.usessl = usessl
        self.connect_event = Event()
        SingleServerIRCBot.__init__(self, [(network, port , password)], nickname, realname, username)
        self.connect_event = Event()

    def on_welcome(self, serv, ev):
        print "Connected to server"
        serv.join(self.channel)
        print "Joined channel: " + self.channel

        self.ircserver = serv
        self.connect_event.set()

    def wait_for_connect(self):
        self.connect_event.wait()

    def show_info(self, serv, user):
        serv.privmsg(user, "LogReader, version: " + VERSION)
        serv.privmsg(user, "Blue Software, 2016")
        serv.privmsg(user, "More infos: https://bluecode.fr")

    def on_privmsg(self, serv, ev):
        msg = ev.arguments()[0]
        user = irclib.nm_to_n(ev.source())

        print "Got private message: \"" + msg +"\", from: \"" + user + "\""
        self.show_info(serv, user)

    def log(self, log):
        for line in log.split('\n'):
            self.ircserver.privmsg(self.channel, line)
