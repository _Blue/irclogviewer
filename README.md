#IRCLogViewer
Send Apache logs through IRC

This python script watches apache's log file and sends new logs to an IRC
channel.

The detection of "new" logs is based on dates within the log file, not  
the file's last modification date.

##Configuration
This script is configured by the file 'logreader.cfg'.

This file contains the following instructions:

* *network [network_address]*: **The name of the IRC server to connect to**
* *port [port_value]*: **The port of the IRC server**
* *usessl*: **Add this line if you want to use SSL**
* *user*: **The username to use**
* *nick*: **The nickname to use**
* *real*: **The realname to use**
* *pass*: **The IRC server password, if needed**
* *channel*: **The channel to join, it should include the #**
* *watch*: **Add a file to the watch list, this line may appear more than once**
* *wait*: **Number of miliseconds to wait between two polls**
* *exclude*: **A regexp that is used to exlude some user agents from the send list**

For example, if you want exclude Google, Yahoo, Bing and Baidu bots, you
can use:

`exclude (^.*[bB]ot$)|(Baiduspider)|(Yahoo! Slurp)`

You can look at the 'logreader.cfg.example' to have a valid example.

##Dependencies
This script depends on the following packages:

* apache\_log\_parser
* dateutils

These packages can be installed using pip.

##Copyright
You are allowed to use, redistribute and modify this file as you wish.