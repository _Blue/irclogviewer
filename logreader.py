#!/usr/bin/python2
# -*- coding: utf-8 -*-

#***********************************************************************************
#This file is part of the LogReader project                                        *
#                                                                                  *
#This program is based on irclib and ircbot                                        *
#You are allowed to use, modify and redistribute this file as you like             *
#Note that this code comes without any guarantee and may be used at your own risks *
#                                                                                  *
#Blue Software, 2016                                                               *
#For more infos: https://bluecode.fr                                               *
#***********************************************************************************

import apache_log_parser
import time
import threading
import logging
import sys
import re

from irchandler import IRCHandler
from datetime import datetime
from dateutil import parser

# default appache log format (thanks stack overflow)
APACHE_COMBINED="%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\""
APACHE_COMMON="%h %l %u %t \"%r\" %>s %b"
CONFIG_FILE = "logreader.cfg"
DEFAULT_WAIT_TIME = 1000
DEFAULT_TS_FILE = 'last_ts'


class LogReader():

    def __init__(self, ts_file, irc_client, wait, files, regexpr):
        self.ts_file = ts_file
        self.irc_client = irc_client
        self.wait = wait
        self.files = files
        self.re = re.compile(regexpr)

    def read_saved_ts(self):
        try:
            ts_file = open(self.ts_file, 'r')
            self.ts = int(ts_file.read())
        except:
            self.ts = 0

    #Extract timestamp from log line
    def read_ts(self, line):
        date = line['time_received']
        timestamp = time.mktime(parser.parse(unicode(line['time_received_isoformat'])).timetuple())
        return timestamp

    def read_file(self, file_name, ts, pattern=APACHE_COMBINED):
        line_parser = apache_log_parser.make_parser(pattern)

        #Read log file
        file = open(file_name, 'r')
        lines = file.readlines()
        file.close()

        if len(lines) == 0:
            return 0

        #Find first line that haven't been process yet
        #Start by the end to optimize
        #Dichotomy search could be considered

        start = len(lines) - 1
        while start >= 0:
            line_ts = self.read_ts(line_parser(lines[start]))
            if line_ts <= self.ts:
                start += 1 #Make sure it won't send the last line
                break
            start -= 1

        start = max(0, start) #In case its value is -1

        #Starting from now, send all lines that match the pattern
        while start < len(lines):
            line_data = line_parser(lines[start])
            if self.re.match(line_data['request_header_user_agent__browser__family']) == None:
                self.send_log(lines[start])
            start += 1

        return max(ts, self.read_ts(line_parser(lines[len(lines) - 1])))

    def send_log(self, log_data):
        if self.irc_client == None: #Case of console mode
            print(log_data)
        else:
            self.irc_client.log(log_data) #Send through IRC client

    def start(self):
        #Load last processed timestamp
        self.read_saved_ts()
        ts = self.ts
        while True:
            for file in self.files:
                try:
                    ts = max(self.read_file(file, ts), ts)
                except Exception as e:
                    if not self.irc_client == None:
                        self.irc_client.log("Error while reading \"" + file + "\", stop")
                        self.irc_client.log(str(e))
                    logging.exception("Error while reading \"" + file + "\"")
                    return
            self.ts = ts
            time.sleep(self.wait // float(1000))

            #Save current timestamp
            open(self.ts_file, 'w').write(str(int(ts)))

if __name__ == "__main__":
    port = 6667 #Default irc port
    password = "" #No password by default
    ts_file = DEFAULT_TS_FILE
    files = []
    waittime = DEFAULT_WAIT_TIME
    usessl = False #No ssl by default
    regexp = ''
    try:
        file = open(CONFIG_FILE, "r")
        data = file.read().split('\n')
        for line in data:
            if line.startswith("network "):
                network = line[8:]
            elif line.startswith("port "):
                port = int(line[5:])
            elif line.startswith("user "):
                user = line[5:]
            elif line.startswith("nick "):
                nick = line[5:]
            elif line.startswith("real "):
                realname = line[5:]
            elif line.startswith("pass "):
                password = line[5:]
            elif line.startswith("channel "):
                channel = line[8:]
            elif line.startswith("usessl"):
                usessl=True
            elif line.startswith("ts_file "):
                ts_file = line[8:]
            elif line.startswith("watch "):
                files.append(line[6:])
            elif line.startswith("wait "):
                waittime = int(line[5:])
            elif line.startswith("exclude "):
                regexp = line[8:]
            elif not line == "":#In case of a line return in the end of the file
                print("Unknown command: \"" + line + "\", exiting")
                exit()
    except:
        print("Error while reading configuration file \"" + CONFIG_FILE + "\", exiting")
        exit()
    try:
        handler = IRCHandler(network, port, password, channel, nick, realname, user, usessl)
    except NameError:
        print("Missing parameter in configuration file, exiting")
        exit()

    if len(sys.argv) > 1 and sys.argv[1] == '-c': #Run in console mode
        reader = LogReader('last_ts', None, waittime, files, regexp)
        reader.start()
        exit()

    #Run in IRC mode
    print("Connecting to : " + network + ":" + str(port))
    threading.Thread(target = handler.start).start()#Start IRC handling in a separate thread

    #Wait for IRC client to be connected
    handler.wait_for_connect()

    print("Connected")
    reader = LogReader(ts_file, handler, waittime, files, regexp)
    reader.start()
